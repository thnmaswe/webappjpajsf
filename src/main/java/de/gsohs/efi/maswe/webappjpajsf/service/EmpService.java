package de.gsohs.efi.maswe.webappjpajsf.service;

import java.util.List;

import javax.ejb.Local;

import de.gsohs.efi.maswe.webappjpajsf.model.Department;
import de.gsohs.efi.maswe.webappjpajsf.model.Employee;
/**
 * defines all business methods of this service fassade.
 * @author Matthias Roth
 *
 */
@Local
public interface EmpService {

	Employee getEmployeeById(Integer id);
	List<Employee> getAllEmployees();
	List<Department> getAllDepartments();
	List<Employee> getEmployeesByDepartment(Integer deptno);
	void saveEmployee(Employee employee);
}