package de.gsohs.efi.maswe.webappjpajsf.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="job")
@Table(name="JOBS")
public class Job {

	@Id
	@Column(name="JOB_ID", length=10)
	private String jobId;
	@Column(name="JOB_TITLE", length=35, nullable=false)
	private String jobTitle;
	@Column(name="MIN_SALARY",scale=6)
	private Integer minSalary;
	@Column(name="MAX_SALARY",scale=6)
	private Integer maxSalary;
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public Integer getMinSalary() {
		return minSalary;
	}
	public void setMinSalary(Integer minSalary) {
		this.minSalary = minSalary;
	}
	public Integer getMaxSalary() {
		return maxSalary;
	}
	public void setMaxSalary(Integer maxSalary) {
		this.maxSalary = maxSalary;
	}
	
	
}
