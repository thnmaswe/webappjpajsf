package de.gsohs.efi.maswe.webappjpajsf.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity(name="region")
@Table(name="REGIONS")
public class Region {
	
	@Id
	@Column(name="REGION_ID")
	private Long regionId;
	
	@Column(name="REGION_NAME", length=25)
	private String name;

	public Long getRegionId() {
		return regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	@Size(min=3,max=25,message="string must contain 3 to 25 characters")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}