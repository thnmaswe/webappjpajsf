package de.gsohs.efi.maswe.webappjpajsf.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.gsohs.efi.maswe.webappjpajsf.model.Employee;

/**
 * implementation if the {@link EmpDao} interface with JPA
 * @author Matthias Roth
 */
@SuppressWarnings("ALL")
@Stateless
public class EmpDaoBean implements EmpDao {
	
	// injects a JPA entity manager instance into this bean.
	// the entity manager factory is defined in applicationContext.xml
	@PersistenceContext
	EntityManager em;
	

	/* (non-Javadoc)
	 * @see de.gsohs.efi.maswe.webappjpajsf.dao.EmpDao#findById(java.lang.Integer)
	 */
	public Employee findById(Integer id) {
		return em.find(Employee.class, id);
	}

	
	/* (non-Javadoc)
	 * @see de.gsohs.efi.maswe.webappjpajsf.dao.EmpDao#findAll()
	 */
	@SuppressWarnings("unchecked")
	public List<Employee> findAll() {
		Query q = em.createQuery("SELECT e FROM employee e order by e.lastName asc");
		return (List<Employee>) q.getResultList();
	}
	
	
	/* (non-Javadoc)
	 * @see de.gsohs.efi.maswe.webappjpajsf.dao.EmpDao#findByDeptNo(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	public List<Employee> findByDeptNo(Integer deptno) {
		// creates a query object from entity manager with named parameters 
		Query q = em.createQuery("SELECT e FROM employee e WHERE e.department.departmentId= :deptno");
		// assignes the named param deptno with the integer value given to this method
		q.setParameter("deptno", deptno);
		// fire the query against db and returns entities
		return  (List<Employee>) q.getResultList();
	}

	
	/* (non-Javadoc)
	 * @see de.gsohs.efi.maswe.webappjpajsf.dao.EmpDao#saveEmployee(de.gsohs.efi.maswe.webappjpajsf.model.Employee)
	 */
	public void saveEmployee(Employee employee) {
		// called to merge all properties in the given detached entity with 
		// record in database and thus save the change.
		em.merge(employee);
	}

}
