package de.gsohs.efi.maswe.webappjpajsf.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * JPA Entity for the EMPLOYEES database table. Annotations in this entity map object
 * properties to database table and columns. See the comments in source code for explanations.
 * Further this object has Bean Validation annotations on method signatures for web-based
 * validation
 * @author Matthias Roth
 *
 */
// marks this object as JPA entity and give it an explicit name for EJBQL queries
@Entity(name="employee")
// map this object to EMPLOYEES table
@Table(name="EMPLOYEES")
@SuppressWarnings("unused")
public class Employee {

	// marks this field as primary key
	@Id
	// this id is generated and comes from a sequence
	@GeneratedValue(generator="EMPLOYEES_SEQ_GEN",strategy=GenerationType.SEQUENCE)
	// definition of the database sequence and its properties
	@SequenceGenerator(name="EMPLOYEES_SEQ_GEN",sequenceName="EMPLOYEES_SEQ", allocationSize=1)
	// column name and size of NUMBER datatype
	@Column(name="EMPLOYEE_ID",scale=6)
	private Integer employeeId;
	
	// column name and size of VARCHAR2 datatype
	@Column(name="FIRST_NAME", length=20)
	private String firstName;
	
	// column name and size of VARCHAR2 datatype
	@Column(name="LAST_NAME", length=25)
	private String lastName;
	
	// column name and size of VARCHAR2 datatype. give it a unique index and mark it as NOT NULL
	@Column(name="EMAIL", length=25, nullable=false, unique=true)
	private String email;
	
	// column name and size of VARCHAR2 datatype
	@Column(name="PHONE_NUMBER", length=20)
	private String phoneNumber;
	
	// column name and mark it as NOT NULL
	@Column(name="HIRE_DATE", nullable=false)
	// mapping of Java Date and SQL Date type
	@Temporal(TemporalType.DATE)
	private Date hireDate;
	
	// column name and floating point precision in database
	@Column(name="SALARY", scale=8, precision=2)
	private Float salary;
	
	// this is a relationship to an entity. Here it is self referential
	@ManyToOne
	// explicit definition of column names in owning and foreign side of this association
	@JoinColumn(name="MANAGER_ID", referencedColumnName="EMPLOYEE_ID")
	private Employee manager;
	
	// this is a relationship to an entity.
	@ManyToOne
	// explicit definition of column names in owning and foreign side of this association
	@JoinColumn(name="DEPARTMENT_ID", referencedColumnName="DEPARTMENT_ID")
	private Department department;
	
	
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	// Bean validations
	// only allow ascii letters in inputs
	@Pattern(regexp = "[A-Za-z]*", message = "must contain only ASCII letters")
	// size must not exceed 20 characters
	@Size(min=2,max=20,message="String must be at least 2 and maximal 20 characters in length")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	// Bean validations
	@Pattern(regexp = "[A-Za-z]*", message = "must contain only ASCII letters")
	@Size(min=2,max=25,message="String must be at least 2 and maximal 25 characters in length")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Date getHireDate() {
		return hireDate;
	}
	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}
	public Float getSalary() {
		return salary;
	}
	public void setSalary(Float salary) {
		this.salary = salary;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public Employee getManager() {
		return manager;
	}
	public void setManager(Employee manager) {
		this.manager = manager;
	}
	
}
