package de.gsohs.efi.maswe.webappjpajsf.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name="location")
@Table(name="LOCATIONS")
public class Location {

	@Id
	@SequenceGenerator(sequenceName="LOCATIONS_SEQ",name="LOCATIONS_SEQ_GEN",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="LOCATIONS_SEQ_GEN")
	@Column(name="LOCATION_ID",scale=4)
	private Integer locationId;
	
	@Column(name="STREET_ADDRESS",length=40)
	private String streetAddress;
	
	@Column(name="POSTAL_CODE",length=12)
	private String postalCode;
	
	@Column(name="CITY",length=30,nullable=false)
	private String city;
	
	@Column(name="STATE_PROVINCE",length=25)
	private String stateProvince;
	
	@ManyToOne
	@JoinColumn(name="COUNTRY_ID",referencedColumnName="COUNTRY_ID")
	private Country country;

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
}
