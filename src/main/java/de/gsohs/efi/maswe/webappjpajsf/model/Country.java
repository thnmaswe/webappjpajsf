package de.gsohs.efi.maswe.webappjpajsf.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("unused")
@Entity(name="country")
@Table(name="COUNTRIES")
public class Country {
	
	@Id
	@Column(name="COUNTRY_ID",length=2)
	private String countryId;
	
	@Column(name="COUNTRY_NAME", length=40)
	private String countryName;
	
	@ManyToOne
	@JoinColumn(name="REGION_ID", referencedColumnName="REGION_ID")
	private Region region;

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}
}