package de.gsohs.efi.maswe.webappjpajsf.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * 
 * JPA Entity for the DEPARTMENTS database table. Annotations in this entity map object
 * properties to database table and columns. See the comments in source code for explanations.
 * 
 * @author Matthias Roth
 * 
 *
 */

@SuppressWarnings("unused")
@Entity(name="department")
@Table(name="DEPARTMENTS")
public class Department {
	
	// marks this field as primary key
	@Id
	// this id is generated and comes from a sequence
	@GeneratedValue(generator="DEPARTMENTS_SEQ_GEN",strategy=GenerationType.SEQUENCE)
	// definition of the database sequence and its properties
	@SequenceGenerator(name="DEPARTMENTS_SEQ_GEN",sequenceName="DEPARTMENTS_SEQ", allocationSize=1)
	// column name and size of NUMBER datatype
	@Column(name="DEPARTMENT_ID",scale=4)
	private Integer departmentId;
	
	// column name and size of VARCHAR2 datatype. Mark it as NOT NULL
	@Column(name="DEPARTMENT_NAME", length=30, nullable=false)
	private String departmentName;
	
	// this is a relationship to an entity.
	@ManyToOne
	// explicit definition of column names in owning and foreign side of this association
	@JoinColumn(name="MANAGER_ID", referencedColumnName="EMPLOYEE_ID")
	private Employee manager;
	
	// this is a relationship to an entity.
	@ManyToOne
	// explicit definition of column names in owning and foreign side of this association
	@JoinColumn(name="LOCATION_ID",referencedColumnName="LOCATION_ID")
	private Location location;
	
	public Integer getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public Employee getManager() {
		return manager;
	}
	public void setManager(Employee manager) {
		this.manager = manager;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	
}
