package de.gsohs.efi.maswe.webappjpajsf.dao;

import java.util.List;

import javax.ejb.Local;

import de.gsohs.efi.maswe.webappjpajsf.model.Employee;
/**
  * Data Access Object interface for {@link Employee} entities.
 * @author Matthias Roth
 */
// the local Annotation marks this interface as EJB local Interface
@Local
public interface EmpDao {
	/**
	 * gets a specific employee by primary key
	 * @param id of a specific employee
	 * @return a employee entity instance
	 */
	 Employee findById(Integer id);
	/**
	 * @return a list of all employees in database
	 */
	 List<Employee> findAll();
	/**
	 * gets employees working in a given department
	 * @param deptno of a department
	 * @return a list of employees assigned to the department
	 */
	 List<Employee> findByDeptNo(Integer deptno);
	/**
	 * saves an employee entity to database
	 * @param employee to be merged
	 */
	 void saveEmployee(Employee employee);
}
