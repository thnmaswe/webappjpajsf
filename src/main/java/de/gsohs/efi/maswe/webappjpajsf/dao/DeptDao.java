package de.gsohs.efi.maswe.webappjpajsf.dao;

import java.util.List;

import javax.ejb.Local;

import de.gsohs.efi.maswe.webappjpajsf.model.Department;
/**
 * Data Access Object interface for {@link Department} entities
 * @author Matthias Roth
 *
 */
//the local Annotation marks this interface as EJB local Interface
@Local
public interface DeptDao {
	/**
	 * 
	 * @return a list of all departments in the database
	 */
	List<Department> findAll();
}
