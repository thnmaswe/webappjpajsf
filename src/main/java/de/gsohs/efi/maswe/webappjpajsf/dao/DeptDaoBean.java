package de.gsohs.efi.maswe.webappjpajsf.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.gsohs.efi.maswe.webappjpajsf.model.Department;

/**
 * implementation if the {@link DeptDao} interface with JPA
 * @author Matthias Roth
 */

// marks this object as Stateless EJB
@SuppressWarnings("unused")
@Stateless
public class DeptDaoBean implements DeptDao {
	
	// injects a JPA entity manager instance into this bean.
	// the entity manager factory is defined in applicationContext.xml
	@PersistenceContext
	private EntityManager em;

	
	/* (non-Javadoc)
	 * @see de.gsohs.efi.maswe.webappjpajsf.dao.DeptDao#findAll()
	 */
	@SuppressWarnings("unchecked")
	public List<Department> findAll() {
		// ask the entity manager to create a dynamic EJBQL query
		Query q = em.createQuery("SELECT d FROM department d");
		// executes the query and returns a list of entities e.g. departments
		return (List<Department>) q.getResultList();
	}

}
