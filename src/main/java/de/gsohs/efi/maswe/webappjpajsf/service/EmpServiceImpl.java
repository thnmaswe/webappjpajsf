package de.gsohs.efi.maswe.webappjpajsf.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import de.gsohs.efi.maswe.webappjpajsf.dao.DeptDao;
import de.gsohs.efi.maswe.webappjpajsf.dao.EmpDao;
import de.gsohs.efi.maswe.webappjpajsf.model.Department;
import de.gsohs.efi.maswe.webappjpajsf.model.Employee;

/**
 * Impelementation of the {@link EmpService} interface
 * 
 * @author Matthias Roth
 *
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class EmpServiceImpl implements EmpService {
	
	@EJB
	private EmpDao dao;
	
	@EJB
	private DeptDao deptDao;
	

	public List<Employee> getAllEmployees() {
		return dao.findAll();
	}
	
	public List<Employee> getEmployeesByDepartment(Integer deptno) {
		return dao.findByDeptNo(deptno);
	}

	public List<Department> getAllDepartments() {
		return deptDao.findAll();
	}

	public Employee getEmployeeById(Integer id) {
		return dao.findById(id);
	}
	// mark this method as transactional. JPA Transaction boundaries are equal to the duration of
	// this business method call. 
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveEmployee(Employee employee) {
		dao.saveEmployee(employee);
	}
	
	

}
