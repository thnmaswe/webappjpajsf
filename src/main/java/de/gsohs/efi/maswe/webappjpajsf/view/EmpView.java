package de.gsohs.efi.maswe.webappjpajsf.view;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import de.gsohs.efi.maswe.webappjpajsf.model.Department;
import de.gsohs.efi.maswe.webappjpajsf.model.Employee;
import de.gsohs.efi.maswe.webappjpajsf.service.EmpService;

/**
 * The JSF Backing Bean for this application. Comments and api doc can be seen in webappjsf project
 * 
 * @author Matthias Roth
 * @see de.gsohs.efi.maswe.webappjpajsf.view.EmpView
 */
@ManagedBean(name="empView")
@SessionScoped
public class EmpView {
	
	private static final Logger LOG = Logger.getLogger(EmpView.class.getName());
	
	@EJB
	private EmpService service;
	
	private Integer selectedDepartment;
	private Employee selectedEmployee;
	
	public EmpView() {

	}


	public List<Employee> getEmployees() {
		List<Employee> emps;
		if(selectedDepartment != null) {
			emps = service.getEmployeesByDepartment(selectedDepartment);
		} else {
			emps = service.getAllEmployees();
		}
		return emps;
	}

	
	public List<SelectItem> getDepartments() {
		List<SelectItem> items = new ArrayList<>();
		List<Department> depts = service.getAllDepartments();

		for (Department d : depts) {
			items.add(new SelectItem(d.getDepartmentId(),d.getDepartmentName()));
		}
		return items;
	}


	public Integer getSelectedDepartment() {
		return selectedDepartment;
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}


	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}


	public void setSelectedDepartment(Integer selectedDepartment) {
		this.selectedDepartment = selectedDepartment;
	}
	
	public void onUpdateDepartment(ValueChangeEvent evt) {
		LOG.info("department with no: "+((Integer)evt.getNewValue()).toString()+" has been selected");
		this.selectedDepartment = (Integer) evt.getNewValue();
	}
	
	public void onEditEmployee(ActionEvent evt) {
		UIParameter component = (UIParameter) evt.getComponent().findComponent("employeeId");
		Integer empNo = Integer.parseInt(component.getValue().toString());
		LOG.info("Employee with No."+empNo.toString()+" has been selected for editing");
		selectedEmployee = service.getEmployeeById(empNo);
	}
	
	public String saveEmployee() {
		if(selectedEmployee != null) {
			service.saveEmployee(selectedEmployee);
		}
		return "employees";
	}
	
}
